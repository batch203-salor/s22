

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

    
    function register(username){

        let doesUserExist = registeredUsers.includes(username);

        if(doesUserExist){
            alert("Registration Failed. Username already exists!");
        } else {
            registeredUsers.push(username);
            alert("Thank you for registering!");
        }

    }


    function addFriend(username){

        let foundUser = registeredUsers.includes(username);

        if(foundUser){
            friendsList.push(username);
            alert(`You have added ${username} as a friend!`);
        } else {
            alert("User not found.");
        }

    };

    
    function displayFriends(){

        if(friendsList.length > 0){

            friendsList.forEach(function(friend){

                console.log(friend);

            })


        } else {
            alert(`You have ${friendsList.length} friends. Add one first.`);
        }

    };


    function displayNumberOfFriends(){

        if(friendsList.length > 0){

            alert(`You currently have ${friendsList.length} friends.`);


        } else {
            alert(`You have ${friendsList.length} friends. Add one first.`);
        }

    };

    function deleteFriend(){

        if(friendsList.length > 0){

            friendsList.pop();

        } else {
            alert(`You have ${friendsList.length} friends. Add one first.`)
        }

    };

function deleteSpecificFriend(username){


    if(friendsList.length > 0){

        let userIndex = friendsList.indexOf(username);
        
        friendsList.splice(userIndex,1);

    } else {
        alert(`You have ${friendsList.length} friends. Add one first.`)
    }

};